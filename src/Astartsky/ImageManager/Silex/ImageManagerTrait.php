<?php
namespace Astartsky\ImageManager\Silex;

use Astartsky\ImageManager\ImageHelper;
use Astartsky\ImageManager\ImageManager;

trait ImageManagerTrait
{
    /**
     * @return ImageManager
     */
    public function getImageManager()
    {
        return $this['image_manager'];
    }

    /**
     * @return ImageHelper
     */
    public function getImageHelper()
    {
        return $this['image_helper'];
    }
} 