<?php
namespace Astartsky\ImageManager\ImageStorage;

use Astartsky\ImageManager\ImageDefinition\ImageDefinitionInterface;

interface ImageStorageInterface
{
    /**
     * @param ImageDefinitionInterface $image
     * @param string $file
     * @return string
     */
    public function getPath(ImageDefinitionInterface $image, $file);

    /**
     * @param string $file
     * @return string
     */
    public function getSourcePath($file);

    /**
     * @param ImageDefinitionInterface $image
     * @param string $file
     * @return string
     */
    public function getUrl(ImageDefinitionInterface $image, $file);

    /**
     * @param ImageDefinitionInterface $image
     */
    public function prepare(ImageDefinitionInterface $image);
}