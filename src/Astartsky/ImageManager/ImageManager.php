<?php
namespace Astartsky\ImageManager;

use Astartsky\ImageManager\ImageDefinition\ImageDefinitionInterface;
use Astartsky\ImageManager\ImageStorage\ImageStorageInterface;

class ImageManager
{
    /** @var ImageStorageInterface */
    protected $storage;

    /** @var ImageDefinitionInterface[] */
    protected $images = array();

    /**
     * @param ImageStorageInterface $storage
     */
    public function setStorage(ImageStorageInterface $storage)
    {
        $this->storage = $storage;
    }

    /**
     * @param ImageDefinitionInterface $image
     */
    public function register(ImageDefinitionInterface $image)
    {
        $image->setStorage($this->storage);
        $this->images[$image->getName()] = $image;
    }

    /**
     * @param string $name
     * @return ImageDefinitionInterface|null
     */
    public function get($name)
    {
        return isset($this->images[$name]) ? $this->images[$name] : null;
    }
}